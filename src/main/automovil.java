package main;
import java.math.BigDecimal;

public class automovil {

	Integer numllantas;
		
	String marca;
		
	String modelo;
		
	BigDecimal precio;
	
	public automovil () {}
	
	public String imprime(){
		String cadena = "";
		cadena +="\n Numllantas: "+ String.valueOf(getNumllantas());
		cadena +=" \n Marca: "+ getMarca();
		cadena +=" \n Modelo:  "+ getModelo();
		
		return cadena;
		
	}
	

	public int getNumllantas() {
		return numllantas;
	}

	public void setNumllantas(int numllantas) {
		this.numllantas = numllantas;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	
	


}

